import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-list-person.js';

suite('CellsListPerson', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-list-person></cells-list-person>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});
