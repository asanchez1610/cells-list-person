import { LitElement, html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsListPerson-styles.js';
import {
  bbvaEdit,
  bbvaTrash,
} from '@bbva-web-components/bbva-foundations-icons';
import '@bbva-web-components/bbva-core-icon/bbva-core-icon.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-list-person></cells-list-person>
```

##styling-doc

@customElement cells-list-person
*/
export class CellsListPerson extends LitElement {
  static get is() {
    return 'cells-list-person';
  }

  // Declare properties
  static get properties() {
    return {
      items: Array ,
      messageNonData: String
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.items = [];
    this.messageNonData = 'No se encontraron datos para mostrar.';
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-list-person-shared-styles'),
    ];
  }

  onDelete(item) {
    this.dispatchEvent(new CustomEvent('on-delete-person', { 
      detail: item,
      bubbles: true, 
      composed: true
    }))
  }

  onEdit(item) {
    this.dispatchEvent(new CustomEvent('on-edit-person', { 
      detail: item,
      bubbles: true, 
      composed: true
    }))
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <div class="content-table" >
      <table class="table">
        <tr>
          <th>Nombres</th>
          <th>Email</th>
          <th>Teléfono</th>
          <th>&nbsp;</th>
        </tr>
        ${this.items.map(
          (item) =>
            html`
              <tr>
                <td>${item.nombres}</td>
                <td>${item.email}</td>
                <td>${item.telefono}</td>
                <td class="actions-buttons">
                  <button class="gray" @click="${() => this.onEdit(item)}" >
                    <bbva-core-icon icon="${bbvaEdit()}"></bbva-core-icon>Editar
                  </button>
                  <button class="danger" @click="${() => this.onDelete(item)}">
                    <bbva-core-icon icon="${bbvaTrash()}"></bbva-core-icon
                    >Eliminar
                  </button>
                </td>
              </tr>
            `
        )}
      </table>
      </div>
      <div class="non-result" ?hidden="${this.items.length > 0}" >
          ${this.messageNonData}
      </div>
    `;
  }
}
